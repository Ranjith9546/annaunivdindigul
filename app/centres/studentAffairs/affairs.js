'use strict';

angular.module('myApp.studentAffairs', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/studentAffairs', {
    templateUrl: 'centres/studentAffairs/affairs.html',
    controller: 'studentAffairsCtrl'
  });
}])

.controller('studentAffairsCtrl', ['$rootScope', function($rootScope) {
$rootScope.come=true;
     $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

    <!--script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')
   

    <!--Photo viewer(contd. below)-->

   

    <!--Sticky Sidebar-->

   
    $('#sidebar').affix({
        offset: {
            top: 245
        }
    });
    var $body = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;
    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });
   

    
    $(document).ready(function() {
    	
        $('a[href^="#"]').on('click', function(e) {
            e.preventDefault();

            var target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function() {
                window.location.hash = target;
            });
        });
    });
    

    <!--Photo viewer(contd.)-->

    (function(d, t) {
        var g = d.createElement(t),
            s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s)
    }(document, 'script'));

    // Colorbox Call

    $(document).ready(function() {
        $("[rel^='lightbox']").prettyPhoto();
    });
   

}]);