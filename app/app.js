'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version',
  'myApp.admnRegionalOffice',
  'myApp.studentAffairs',
  'myApp.depCivil',
  'myApp.cse',
  'myApp.ece',
  'myApp.eee',
  'myApp.mca',
  'myApp.mech',
  'myApp.mba',
  'myApp.depscienceHumanities',
  'myApp.Campusculture',
  'myApp.CampusSports',
  'myApp.CampusSocial',
  'myApp.contactUs',
  'myApp.Facilitiesaakash',
  'myApp.Facilitiesnptel',
  'myApp.Facilitiesiste',
  'myApp.CampusFacilitiesQuality',
  'myApp.Facilitiesiit',
  'myApp.Facilitiesirs',
  'myApp.Campusgallery',
  /*'myApp.depCivilFac',*/
  'myApp.placement',
  'myApp.stdLogin',
  'myApp.staffLogin',
  'myApp.news',
  'myApp.slider',
  'myApp.placeTraining',
  'myApp.placeAchievers',
  'myApp.placeNews',
  'myApp.placeContact',
  'myApp.slider1',
  'myApp.civilFaculty',
  'myApp.sliderAakash',
  'myApp.slidercad',
  'myApp.sliderfuture',
  'myApp.sliderTamil',
  'myApp.viewFaculty',
/*  'myApp.slider1',*/
  'myApp.centresAffiliation'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/home'});
}]);
