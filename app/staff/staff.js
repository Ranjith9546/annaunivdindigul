angular.module('myApp.staffLogin', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/staffLogin', {
    templateUrl: 'staff/staff.html',
    controller: 'staffLoginCtrl'
  });
}])

.controller('staffLoginCtrl', ['$rootScope',function($rootScope) {
$rootScope.come = false;
    
}]);