'use strict';

angular.module('myApp.civilFaculty', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/civil/faculty', {
    templateUrl: 'faculty/faculty.html',
    controller: 'civilFacultyCtrl'
  });
}])

.controller('civilFacultyCtrl', ['$rootScope', function($rootScope) {
$rootScope.come=true;
     $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

    window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')
     
    $('#sidebar').affix({
        offset: {
            top: 245
        }
    });
    var $body = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;
    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });

    (function(d, t) {
        var g = d.createElement(t),
            s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s)
    }(document, 'script'));
}]);