'use strict';

angular.module('myApp.placeNews', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/placeNews', {
    templateUrl: 'placement/NewsAndEvents/news.html',
    controller: 'placeNewsCtrl'
  });
}])

.controller('placeNewsCtrl', ['$rootScope', function($rootScope) {
$rootScope.come=false;
     $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

    
}]);