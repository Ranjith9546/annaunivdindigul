'use strict';

angular.module('myApp.placeAchievers', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/placeAchievers', {
    templateUrl: 'placement/achievers/achievers.html',
    controller: 'placeAchieversCtrl'
  });
}])

.controller('placeAchieversCtrl', ['$rootScope', function($rootScope) {
$rootScope.come=false;
     $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

    
}]);