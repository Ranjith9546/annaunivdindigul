'use strict';

angular.module('myApp.placeTraining', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/placeTraining', {
    templateUrl: 'placement/training/training.html',
    controller: 'placeTrainingCtrl'
  });
}])

.controller('placeTrainingCtrl', ['$rootScope', function($rootScope) {
$rootScope.come=false;
     $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

    
}]);