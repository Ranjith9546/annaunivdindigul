angular.module('myApp.stdLogin', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/stdLogin', {
    templateUrl: 'student/student.html',
    controller: 'stdLoginCtrl'
  });
}])

.controller('stdLoginCtrl', ['$rootScope',function($rootScope) {
$rootScope.come = false;
    
}]);