'use strict';

angular.module('myApp.depCivil', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/depCivil', {
    templateUrl: 'departments/civil/civil.html',
    controller: 'depCivilCtrl'
  });
}])

.controller('depCivilCtrl', ['$rootScope', function($rootScope) {
    $rootScope.come=true;

    $(window).resize(function() {
    var path = $(this);
    var contW = path.width();
    if(contW >= 751){
        document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
    }else{
        document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
    }
});
$(document).ready(function() {
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
    });
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        var elem = document.getElementById("sidebar-wrapper");
        left = window.getComputedStyle(elem,null).getPropertyValue("left");
        if(left == "200px"){
            document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
        }
        else if(left == "-200px"){
            document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
        }
    });
});

     $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

    window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')
    $('#sidebar').affix({
        offset: {
            top: 245
        }
    });
    var $body = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;
    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });
    $(document).ready(function() {
        $('a[href^="#"]').on('click', function(e) {
            e.preventDefault();

            var target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function() {
                window.location.hash = target;
            });
        });
    });
    

    <!--Photo viewer(contd.)-->

    
    (function(d, t) {
        var g = d.createElement(t),
            s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s)
    }(document, 'script'));

    // Colorbox Call

    $(document).ready(function() {
        $("[rel^='lightbox']").prettyPhoto();
    });


}]);